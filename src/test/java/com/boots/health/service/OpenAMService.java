package com.boots.health.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.boots.health.common.TestConstants;
import com.boots.health.main.HealthSteps;
import com.boots.health.util.GenericRestClient;
import com.boots.health.util.OpenAMUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class OpenAMService {

	private static final Logger logger = Logger.getLogger(OpenAMService.class);

	private String jwtToken = null;

	private String apiKeyForOpenAM = "0CQ1AqmQr2N86m22rVg69AW4Jj9eSgH3";

	private static String apiKey;

	static {
		if (HealthSteps.baseURL.contains("paas")) {
			apiKey = "Gv1cY8SBZ4kGImVGcc8ZGe6W5mxFnCIi";
		} else {
			apiKey = "0CQ1AqmQr2N86m22rVg69AW4Jj9eSgH3";
		}

	}

	private String openAmUserName = "apiuser";

	private String openAmPassword = "spafrazEb2";

	private String openAmUrl = "http://boots-betalabs-test.apigee.net/iam/json/customer/authenticate";

	public static final ObjectMapper MAPPER = new ObjectMapper();

	public Map<String, String> prepContentHeaders() throws JsonProcessingException, IOException {

		if (jwtToken == null) {
			jwtToken = authenticateSession();
		}

		Map<String, String> contentHeaders = new HashMap<String, String>();
		contentHeaders.put(TestConstants.CONTENT_HDR.API_KEY, apiKey);
		contentHeaders.put(TestConstants.CONTENT_HDR.AUTHORIZATION, "bearer " + jwtToken);
		return contentHeaders;
	}

	public String authenticateSession() throws JsonProcessingException, IOException {
		Map<String, String> openAmHeaders = new HashMap<String, String>();
		openAmHeaders.put(TestConstants.OPEN_AM_HDR.API_KEY, apiKeyForOpenAM);
		openAmHeaders.put(TestConstants.OPEN_AM_HDR.AUTH_UN, openAmUserName);
		openAmHeaders.put(TestConstants.OPEN_AM_HDR.AUTH_PW, openAmPassword);
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(openAmUrl, null, openAmHeaders,
				HttpMethod.POST);

		String token = OpenAMUtils.getValueFromJson(jsonResponse, TestConstants.JSON.TOKEN_ID);

		return getJWTfromToken(token);
	}

	public String getJWTfromToken(String token) {

		logger.info("The token returned from OpenAM is:" + token);
		// The JWT token starts after the second dot in the token
		String pattern = "^.{100}(.*)";
		String jwtToken = null;
		//
		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		// Now create matcher object.
		Matcher m = r.matcher(token);
		if (m.find()) {
			jwtToken = m.group(1);
			int index = jwtToken.indexOf(".*");
			if (index != -1) {
				jwtToken = jwtToken.substring(index + 2, jwtToken.length());
			} else {
				index = jwtToken.indexOf("*");
				jwtToken = jwtToken.substring(index + 1, jwtToken.length());
			}
		}

		logger.info("Extracted JWT Token is:" + jwtToken);

		return jwtToken;
	}

}