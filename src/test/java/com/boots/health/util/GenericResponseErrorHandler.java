package com.boots.health.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;

@Component
public class GenericResponseErrorHandler implements ResponseErrorHandler {

	@Autowired
	private static final Logger LOGGER = LoggerFactory.getLogger(GenericResponseErrorHandler.class);

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		LOGGER.error("Response error: {} {}", response.getStatusCode(), response.getStatusText());
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return response.getStatusCode().value() >= 400;
	}
}