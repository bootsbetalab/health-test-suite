package com.boots.health.util;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.boots.health.exception.HttpException;

@Component
public class GenericRestClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenericRestClient.class);

	

	public String doRestCallWithPayload(String url, String payload, Map<String, String> incomingHeaders,
			HttpMethod method) {
		long startTime = System.currentTimeMillis();
		
		//System.setProperty("http.proxyHost", "165.225.104.32");
        //System.setProperty("http.proxyPort", "10223");
    	//System.setProperty("http.proxyUser", "ustr\u31794");
        //System.setProperty("http.proxyPassword", "Hotmail14314");
		
		LOGGER.info("About to make a rest call to: " + url);

		ResponseEntity<String> response_entity = new HcdRestTemplate().exchange(url, method,
				getHttpEntity(payload, incomingHeaders), String.class);
		LOGGER.info("Call made. Response code:" + response_entity.getStatusCode());
		LOGGER.debug("Response body:" + response_entity.getBody());

		LOGGER.debug("Rest Call duration: " + (System.currentTimeMillis() - startTime));
		// we want all the subservices exception to be propagated as they are,
		// code + response payload
		if (response_entity.getStatusCode().value() >= 400) {
			throw new HttpException(response_entity.getBody(), response_entity.getStatusCode(),
					GenericRestClient.class);
		}
		return response_entity.getBody();
	}

	private HttpEntity<String> getHttpEntity(String payload, Map<String, String> incomingHeaders) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAll(incomingHeaders);
		HttpEntity<String> entity = null;
		if (payload == null) {
			LOGGER.debug("No payload");
			entity = new HttpEntity<String>(headers);
		} else {
			LOGGER.info("Request payload:" + payload);
			entity = new HttpEntity<String>(payload, headers);
		}
		return entity;
	}

	@PostConstruct
	public void prepare() {
		new HcdRestTemplate().setErrorHandler(new GenericResponseErrorHandler());
	}
}
