package com.boots.health.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HealthTestUtils {
	public String getFileContents(String fileName) throws IOException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(fileName);
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String currentLine;
		StringBuilder queryStringBuilder = new StringBuilder();
		while ((currentLine = br.readLine()) != null) {
			queryStringBuilder.append(currentLine);
		}
		return queryStringBuilder.toString();
	}

}
