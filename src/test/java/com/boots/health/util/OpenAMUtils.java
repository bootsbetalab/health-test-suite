package com.boots.health.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;


public class OpenAMUtils {

	public static final ObjectMapper MAPPER = new ObjectMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(OpenAMUtils.class);
	
	

	public static String getValueFromJson(String json, String elementToFetch)
			throws JsonProcessingException, IOException {
		JsonNode jsonNode = MAPPER.readTree(json);
		String jsonString = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
		return extractDataOrNull(jsonString, elementToFetch);
	}

	public static <T> String convertJsonToString(T jsonToConvert) throws JsonProcessingException {
		return MAPPER.writer().writeValueAsString(jsonToConvert);
	}

	public static String extractDataOrNull(String response, String element) {
		String s = null;
		try {
			s = extractData(response, element);
		} catch (Exception e) {
			LOGGER.error("Exception: " + e);
		}
		return s;
	}

	private static String extractData(String response, String element) {
		return removeBrackets(JsonPath.read(response, element).toString());
	}

	public static String removeBrackets(String data) {
		return data.replace("[", "").replace("]", "");
	}

}