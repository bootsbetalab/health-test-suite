package com.boots.health.common;

public class TestConstants {
	public static class OPEN_AM_HDR {
		public static final String ACCEPT_LANG_HDR = "Accept-Language";
		public static final String AUTH_UN = "X-OpenAM-Username";
		public static final String AUTH_PW = "X-OpenAM-Password";
		public static final String API_KEY = "apikey";
		public static final String CONTENT_TYPE = "Content-Type";
		public static final String APP_JSON = "application/json";
	}
	public static class CONTENT_HDR {
		public static final String ACCEPT_LANG_HDR = "Accept-Language";
		public static final String API_KEY = "apikey";
		public static final String CONTENT_TYPE = "Content-Type";
		public static final String APP_JSON = "application/json";
		public static final String AUTHORIZATION = "Authorization";
		
	}

	public static class JSON {
		public static final String TELECOM = "telecom";
		public static final String SYSTEM = "system";
		public static final String VALUE = "value";
		public static final String EMAIL = "email";
		public static final String TOKEN_ID = "tokenId";
		public static final String USER_STATUS = "inetUserStatus";
	}
}
