package com.boots.health.main;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import com.boots.health.service.OpenAMService;
import com.boots.health.util.GenericRestClient;
import com.boots.health.util.HealthTestUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HealthSteps {

	private static final Logger logger = LoggerFactory.getLogger(HealthSteps.class);

	public static String baseURL = "http://boots-betalabs-test.apigee.net/paas-test/";

	private OpenAMService openAMService = new OpenAMService();

	private String patientId = null;
	private String goalId = null;
	private String encounterId = null;
	private String observationId = null;
	private String userPrefId = null;

	@Given("^A patient with email id (.*)")
	public void a_patient_with_given_name_Integration_and_family_name_Integration_and_email_id_integrationtester_gmail_com_and_birthdate_as(
			String emailId) throws Throwable {

		// To bypass validation for duplicate email id, prefix email with a rand
		int num = new Random().nextInt(10000);

		String url = baseURL + "v1/fhir/patient/";

		String payload = new HealthTestUtils().getFileContents("patientCreate.json");

		payload = payload.replace("<emailAddress>", num + emailId);

		// Make the API call
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, payload,
				openAMService.prepContentHeaders(), HttpMethod.POST);

		Assert.assertNotNull(url + "- REST API call failed! The microservice is probably down.", jsonResponse);

		Pattern r = Pattern.compile("\"id\"\\s*:\\s*\"(.*)\"\\s*,\\s*\"extension");
		Matcher m = r.matcher(jsonResponse);
		if (m.find()) {
			patientId = m.group(1);
			logger.info("patientId created is:" + patientId);
		}

	}

	@Given("^There are stores available in the system$")
	public void there_are_stores_available_in_the_system() throws Throwable {

		String url = baseURL + "v1/store";
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, null,
				openAMService.prepContentHeaders(), HttpMethod.GET);
		// Check if we got London Strand store back
		Assert.assertTrue(url + "- REST API call failed! The microservice is probably down.",
				jsonResponse.contains("1629"));

	}

	@Given("^Content is available$")
	public void content_is_available() throws Throwable {
		String url = baseURL + "v1/content?tag=week1&type=video";
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, null,
				openAMService.prepContentHeaders(), HttpMethod.GET);
		// Check if we got the motivational video back
		Assert.assertTrue(url + "- REST API call failed! The microservice is probably down.",
				jsonResponse.contains("motivational"));

	}

	@Given("^Products are available in Health Shop$")
	public void products_are_available_in_Health_Shop() throws Throwable {

		String url = baseURL + "v1/product-grouping?id:contains=Health.Diabetes";
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, null,
				openAMService.prepContentHeaders(), HttpMethod.GET);
		// Check if we got the motivational video back
		Assert.assertTrue(url + "- REST API call failed! The microservice is probably down.",
				jsonResponse.contains("Health.Diabetes"));

	}

	@When("^A new goal is setup for the Patient$")
	public void a_new_goal_is_setup_for_the_Patient() throws Throwable {

		String url = baseURL + "v1/hcd-goal?patientId=";
		url = url + patientId;

		String payload = new HealthTestUtils().getFileContents("goalCreate.json");

		// Make the API call
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, payload,
				openAMService.prepContentHeaders(), HttpMethod.POST);

		Pattern r = Pattern.compile("\"id\"\\s*:\\s*\"(.*)\"\\s*,\\s*\"goalDef");
		Matcher m = r.matcher(jsonResponse);
		if (m.find()) {
			goalId = m.group(1);
			logger.info("goalId created is:" + goalId);
		}

		Assert.assertNotNull(url + "- REST API call failed! The microservice is probably down.", jsonResponse);

	}

	@When("^A new encounter is created for the Patient$")
	public void a_new_encounter_is_created_for_the_Patient() throws Throwable {
		String url = baseURL + "v1/fhir/encounter";

		String payload = new HealthTestUtils().getFileContents("encounterCreate.json");
		payload = payload.replace("<patientId>", patientId);

		// Make the API call
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, payload,
				openAMService.prepContentHeaders(), HttpMethod.POST);

		Pattern r = Pattern.compile("\"id\"\\s*:\\s*\"(.*)\"\\s*,\\s*\"extension");
		Matcher m = r.matcher(jsonResponse);
		if (m.find()) {
			encounterId = m.group(1);
			logger.info("encounterId created is:" + encounterId);
		}

		Assert.assertNotNull(url + "- REST API call failed! The microservice is probably down.", jsonResponse);

	}

	@When("^A new observation is recorded for the Patient$")
	public void a_new_observation_is_recorded_for_the_Patient() throws Throwable {

		String url = baseURL + "v1/fhir/observation";

		String payload = new HealthTestUtils().getFileContents("observationCreate.json");
		payload = payload.replace("<patientId>", patientId).replace("<encounterId>", encounterId);

		// Make the API call
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, payload,
				openAMService.prepContentHeaders(), HttpMethod.POST);

		Pattern r = Pattern.compile("\"id\"\\s*:\\s*\"(.*)\"\\s*,\\s*\"extension");
		Matcher m = r.matcher(jsonResponse);
		if (m.find()) {
			observationId = m.group(1);
			logger.info("observationId created is:" + observationId);
		}

		Assert.assertNotNull(url + "- REST API call failed! The microservice is probably down.", jsonResponse);

	}

	@When("^A new user preference is set for the Patient$")
	public void a_new_user_preference_is_set_for_the_Patient() throws Throwable {
		String url = baseURL + "v1/user-preferences/user/" + patientId + "/preferences";

		String payload = new HealthTestUtils().getFileContents("userPrefCreate.json");

		// Make the API call
		String jsonResponse = new GenericRestClient().doRestCallWithPayload(url, payload,
				openAMService.prepContentHeaders(), HttpMethod.POST);

		Pattern r = Pattern.compile("\"userPrefId\"\\s*:\\s*\"(.*)\"\\s*,\\s*\"category");
		Matcher m = r.matcher(jsonResponse);
		if (m.find()) {
			userPrefId = m.group(1);
			logger.info("userPrefId created is:" + userPrefId);
		}

		Assert.assertNotNull(url + "- REST API call failed! The microservice is probably down.", jsonResponse);

	}

	@Then("^Delete all Test Data which was created$")
	public void delete_all_Test_Data_which_was_created() throws Throwable {

		String url = baseURL + "v1/fhir/observation/" + observationId;
		new GenericRestClient().doRestCallWithPayload(url, null, openAMService.prepContentHeaders(), HttpMethod.DELETE);

		url = baseURL + "v1/fhir/encounter/" + encounterId;
		new GenericRestClient().doRestCallWithPayload(url, null, openAMService.prepContentHeaders(), HttpMethod.DELETE);

		url = baseURL + "v1/fhir/goal/" + goalId;
		new GenericRestClient().doRestCallWithPayload(url, null, openAMService.prepContentHeaders(), HttpMethod.DELETE);

		url = baseURL + "v1/fhir/patient/" + patientId;
		new GenericRestClient().doRestCallWithPayload(url, null, openAMService.prepContentHeaders(), HttpMethod.DELETE);

	}
}
