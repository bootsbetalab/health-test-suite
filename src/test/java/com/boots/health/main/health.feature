Feature: Health
To test all micro services related to Health

Scenario: Test all microservices

Given A patient with email id integrationtester@gmail.com
And There are stores available in the system
And Content is available
And Products are available in Health Shop


When A new goal is setup for the Patient
And A new encounter is created for the Patient
And A new observation is recorded for the Patient
And A new user preference is set for the Patient

Then Delete all Test Data which was created