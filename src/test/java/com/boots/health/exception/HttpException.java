package com.boots.health.exception;

import org.springframework.http.HttpStatus;

public class HttpException extends TestException {

	private static final long serialVersionUID = 1L;

	public HttpStatus httpStatus;

	public Class<?> clazzName;

	public HttpException(String message, HttpStatus httpStatus, Class<?> clazzName) {
		super(message);
		this.httpStatus = httpStatus;
		this.clazzName = clazzName;
	}
}