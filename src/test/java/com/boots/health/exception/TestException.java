package com.boots.health.exception;

import org.springframework.http.HttpStatus;

public class TestException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public HttpStatus httpStatus;

	public Class<?> clazzName;

	public TestException(String message, Throwable t) {
		super(message, t);
	}

	public TestException(String message) {
		super(message);
	}
}